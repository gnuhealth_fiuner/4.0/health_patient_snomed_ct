#  -*- coding: utf-8 -*-
# This file is part health_patient_snomed_ct module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import health_patient_snomed_ct
from . import configuration

from .wizard import medication_load_snomed
from .wizard import condition_load_snomed


def register():
    Pool.register(
        configuration.SnomedConfig,
        health_patient_snomed_ct.PatientMedication,
        health_patient_snomed_ct.PatientCondition,
        health_patient_snomed_ct.SnomedMedication,
        health_patient_snomed_ct.SnomedCondition,
        module='health_patient_snomed_ct', type_='model')
    Pool.register(
        medication_load_snomed.LoadSnomedMedication,
        condition_load_snomed.LoadSnomedCondition,
        module='health_patient_snomed_ct', type_='wizard')
