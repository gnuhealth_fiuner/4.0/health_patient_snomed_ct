# -*- coding: utf-8 -*-
# This file is part health_patient_snomed_ct module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.model import fields, ModelSQL, ModelView, ModelSingleton, Unique
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Not, Bool, Or

from ..health_snomed_ct.health_snomed_ct import SnomedTemplate


class PatientMedication(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.medication'

    snomed_medication =  fields.One2Many(
        'gnuhealth.patient.medication.snomed_medication',
        'medication', 'Snomed Medication',
        states={'invisible': Bool(Eval('use_default_medication'))},
        depends=['use_default_medication'])
    use_default_medication = fields.Boolean('Use Default Medication')

    @staticmethod
    def default_use_default_medication():
        return False

    @classmethod
    @ModelView.button_action('health_patient_snomed_ct.wizard_load_snomed_medication')
    def load_snomed_medication(cls, medications):
        pass

    @classmethod
    def __setup__(cls):
        super().__setup__()
        #cls.medication_domain.states['invisible'] = Or(
            #cls.medication_domain.states.get('invisible', False),
            #Not(Bool(Eval('use_default_medication'))))
        #if 'use_default_medication' not in cls.medication_domain.depends:
            #cls.medication_domain.depends.append('use_default_medication')
        #cls.medication.states['invisible'] = Or(
            #cls.medication.states.get('invisible', False),
            #Not(Bool(Eval('use_default_medication'))))
        #if 'use_default_medication' not in cls.medication.depends:
            #cls.medication.depends.append('use_default_medication')

        cls._buttons.update({
                'load_snomed_medication': {
                    'invisible': Bool(Eval('use_default_medication'))
                    },
                })


class PatientCondition(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.disease'

    
    snomed_condition =  fields.One2Many(
        'gnuhealth.patient.disease.snomed_condition',
        'condition', 'Snomed Condition',
        states={'invisible': Bool(Eval('use_default_disease'))},
        depends=['use_default_disease'])    

    use_default_disease = fields.Boolean('Use Default Diseases')

    @staticmethod
    def default_use_default_disease():
        return False

    @classmethod
    @ModelView.button_action('health_patient_snomed_ct.wizard_load_snomed_condition')
    def load_snomed_condition(cls, condition):
        pass

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.pathology.required = False

        # cls.disease_domain.states['invisible'] = Or(
        #     cls.disease_domain.states.get('invisible', False),
        #     Not(Bool(Eval('use_default_disease'))))
        # if 'use_default_disease' not in cls.disease_domain.depends:
        #     cls.disease_domain.depends.append('use_default_disease')
        #     cls.disease.states['invisible'] = Or(
        #     cls.disease.states.get('invisible', False),
        #     Not(Bool(Eval('use_default_disease'))))
        # if 'use_default_disease' not in cls.disease.depends:
        #     cls.disease.depends.append('use_default_disease')


        cls._buttons.update({
                'load_snomed_condition': {
                    'invisible': Bool(Eval('use_default_disease'))
                    },
                })


class SnomedMedication(SnomedTemplate, ModelSQL, ModelView):
    'Snomed Medication'
    __name__ = 'gnuhealth.patient.medication.snomed_medication'

    medication = fields.Many2One('gnuhealth.patient.medication',
        'Patient', required=True)

    @classmethod
    def __setup__(cls):
        super(SnomedMedication, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('concept_id_uniq', Unique(t, t.medication, t.concept_id),
                'The concept must be unique for each medication'),
        ]


class SnomedCondition(SnomedTemplate, ModelSQL, ModelView):
    'Snomed Condition'
    __name__ = 'gnuhealth.patient.disease.snomed_condition'

    condition = fields.Many2One('gnuhealth.patient.disease',
        'Patient', required=True)

    @classmethod
    def __setup__(cls):
        super(SnomedCondition, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('concept_id_uniq', Unique(t, t.condition, t.concept_id),
                'The concept must be unique for each condition'),
        ]
