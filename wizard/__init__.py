# -*- coding: utf-8 -*-
# This file is part health_patient_snomed_ct module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from . import medication_load_snomed
from . import condition_load_snomed

