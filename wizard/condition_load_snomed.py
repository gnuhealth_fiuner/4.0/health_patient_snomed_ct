# -*- coding: utf-8 -*-
# This file is part health_patient_snomed_ct module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.wizard import Wizard
from trytond.pool import Pool
from trytond.transaction import Transaction

from trytond.modules.health_snomed_ct.wizard.snomed_templates_wizard \
    import LoadSnomedTemplate


class LoadSnomedCondition(LoadSnomedTemplate, Wizard):
    'Load Snomed Cndition'
    __name__ = 'gnuhealth.patient.disease.snomed_condition.load'

    def default_start(self, fields):
        pool = Pool()
        SnomedConfig = pool.get('gnuhealth.snomed.config')
        start = super().default_start(fields)
        ecl = SnomedConfig(1).ecl_condition
        start['ecl'] = ecl
        return start


    def transition_load(self):
        pool = Pool()
        SnomedCondition = pool.get('gnuhealth.patient.disease.snomed_condition')

        # cargamos las líneas
        load_lines = self._load_lines()
        # se las asignamos al registro actual
        if load_lines:
            for line in load_lines:
                line['condition'] = Transaction().context['active_id']
            SnomedCondition.create(load_lines)
        return 'end'
