GNU Health / PatientSnomedCT

Este módulo permite la integración de GNU Health con SnomedCT.
Por más información, ver el siguiente enlace:
https://www.argentina.gob.ar/salud/snomed

Prerequisitos
-------------

 * Python 3.6 or later (http://www.python.org/)
 * Tryton 6.0 (http://www.tryton.org/)

Configuration
-------------


Soporte
-------

Para más información, o si encuentra algún problema en el módulo,
favor de contactar a:




GESPYTA

saludpublica@ingenieria.uner.edu.ar


Licencia
--------

Ver LICENSE

Copyright
---------

Ver COPYRIGHT
