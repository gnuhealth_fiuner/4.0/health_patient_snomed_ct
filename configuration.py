# -*- coding: utf-8 -*-
# This file is part health_snomed_ct module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta


class SnomedConfig(metaclass=PoolMeta):
    __name__ = 'gnuhealth.snomed.config'

    ecl_condition = fields.Text('Condition ECL')
    ecl_medication = fields.Text('Medication ECL')


    @staticmethod
    def default_ecl_condition():
        ecl = \
            '<<404684003 |hallazgo clinico (hallazgo)| ' \
            + 'OR <272379006 |Event (event)| OR <243796009 ' \
            + '|Situation with explicit context (situation)| ' \
            + 'OR <48176007 |Social context (social concept)| ' \
            + 'OR <71388002 |procedimiento (procedimiento)|'
        return ecl

    @staticmethod
    def default_ecl_medication():
        ecl = \
            '<<404684003 |hallazgo clinico (hallazgo)| ' \
            + 'OR <272379006 |Event (event)| OR <243796009 ' \
            + '|Situation with explicit context (situation)| ' \
            + 'OR <48176007 |Social context (social concept)| ' \
            + 'OR <71388002 |procedimiento (procedimiento)|'
        return ecl
